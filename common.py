# common.py
from __future__ import print_function
from imutils.video import VideoStream
import cv2, os, sys
import argparse
import base64
import imutils
# from time import sleep
# from eventlet.greenthread import sleep
from imutils import face_utils
# import dlib
import numpy as np
from time import sleep

IMG_SIZE = 64
DEPTH = 16
K = WIDTH = 8
MARGIN = 0.4


# DID_GPIO_SETUP = False

# def set_light(value):
#     if value:
#         gpio.output(4, gpio.HIGH)
#     else:
#         gpio.output(4, gpio.LOW)

def take_photo(camera, rotate, width, height, sleept=2.0, iso=0):
    print ("take photo", file=sys.stderr)
    if camera == -1:
        import picamera, picamera.array
        import RPi.GPIO as gpio
        gpio.output(4, gpio.HIGH)
        with picamera.PiCamera(resolution=(width, height)) as camera:
            # camera.resolution = ()
            # camera.iso = iso
            if sleept:
                # print ("sleep", sleep, file=sys.stderr)
                sleep(sleept)
            #ss = camera.exposure_speed
            #camera.shutter_speed = ss
            #camera.exposure_mode = 'off'
            #g = camera.awb_gains
            #camera.awb_mode = 'off'
            #camera.awb_gains = g

            with picamera.array.PiRGBArray(camera) as frame:
                camera.capture(frame, 'bgr')
                # print("got frame", frame)
                current_image_bgr = frame.array
                gray = cv2.cvtColor(current_image_bgr, cv2.COLOR_BGR2GRAY)
                if rotate:
                    gray = imutils.rotate_bound(gray, rotate)
                    current_image_bgr = imutils.rotate_bound(current_image_bgr, rotate)
                else:
                    current_image_bgr = imutils.rotate_bound(current_image_bgr, 0)
                current_image = gray
                # print('Captured %dx%d image' % (
                #         output.array.shape[1], output.array.shape[0]))
                # _, imdata = cv2.imencode(".jpg", gray)
                # data["height"], data["width"] = gray.shape[:2]
                # data["image"] = make_jpeg_data_url(imdata)
                # emit("photo", data)
                # return jsonify(data)
                return current_image, current_image_bgr
    else:
        stream = cv2.VideoCapture(camera)
        (grabbed, frame) = stream.read()
        current_image_bgr = frame
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        if rotate:
            gray = imutils.rotate_bound(gray, rotate)
            current_image_bgr = imutils.rotate_bound(current_image_bgr, rotate)
        current_image = gray
        _, imdata = cv2.imencode(".jpg", gray)
        # data={}
        # data["height"], data["width"] = gray.shape[:2]
        # data["image"] = make_jpeg_data_url(imdata)
        # emit("photo", data)
        stream.release()
        return current_image, current_image_bgr

def get_faces (face_cascade, current_image, scaleFactor=None, minNeighbors=None, minSize=100, use_dlib=False):
    args = dict()
    if scaleFactor:
        args['scaleFactor'] = scaleFactor
    if minNeighbors:
        args['minNeighbors'] = minNeighbors
    if minSize:
        args['minSize'] = (minSize, minSize)
    gray = current_image
    faces = face_cascade.detectMultiScale(gray, **args)
    ret=[]
    for rect in faces:
        x, y, w, h = rect
        d = {'x': int(x), 'y': int(y), 'width': int(w), 'height': int(h)}
        if use_dlib:
            import dlib
            d['rect'] = dlib.rectangle(x, y, x+w, y+h)
        ret.append(d)
    return ret

# def get_landmarks(shape_predictor, current_image, current_faces):
#     shapes = []
#     for rect in current_faces:
#         ss = shape_predictor(current_image, rect)
#         ss = face_utils.shape_to_np(ss)
#         shapes.append([{'x': int(x[0]), 'y': int(x[1])} for x in ss])
#     data = {}
#     data['landmarks'] = shapes
#     return data

def get_emotions(emotion_classifier, current_image, current_faces):
    emotion_target_size = emotion_classifier.input_shape[1:3]
    emotion_offsets = (20, 40)
    emotion_labels = ['angry','disgust','fear','happy','sad','surprise','neutral']

    def preprocess_input(x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    def apply_offsets(face_coordinates, offsets):
        x, y, width, height = face_coordinates
        x_off, y_off = offsets
        return (x - x_off, x + width + x_off, y - y_off, y + height + y_off)

    dd = []
    for face in current_faces:
        # rect = face['rect']
        face_coordinates = (face['x'], face['y'], face['width'], face['height'])
        # x1, y1, x2, y2 = apply_offsets(face_coordinates, emotion_offsets)
        x1, y1, x2, y2 = face_coordinates[0], face_coordinates[1], face_coordinates[0]+face_coordinates[2], face_coordinates[1]+face_coordinates[3]
        gray_face = current_image[y1:y2, x1:x2]
        try:
            gray_face = cv2.resize(gray_face, (emotion_target_size))
        except:
            print ("exception resizing gray_face", file=sys.stderr)
            continue

        gray_face = preprocess_input(gray_face, True)
        gray_face = np.expand_dims(gray_face, 0)
        gray_face = np.expand_dims(gray_face, -1)
        emotion_prediction = emotion_classifier.predict(gray_face)
        # emotion_probability = np.max(emotion_prediction)
        # emotion_label_arg = np.argmax(emotion_prediction)
        # emotion_text = emotion_labels[emotion_label_arg]
        # emotion_window.append(emotion_text)
        d = {}
        max_score = None
        for label, score in zip(emotion_labels, emotion_prediction[0]):
            s = float(score)
            d[label] = s
            if max_score == None or s>max_score:
                max_score = s
                d['label'] = label
        face['emotion'] = d
        face['emotionarray'] = emotion_prediction
        # d['label'] will be the max key with the max score

        # print ("got emotion_prediction", d)
        # dd.append(d)

def get_genderage(genderage_classifier, current_image_bgr, current_faces):
    img = current_image_bgr
    img_h, img_w, _ = np.shape(img)

    # detect faces using dlib detector
    # detected = detector(input_img, 1)
    detected = current_faces
    faces = np.empty((len(detected), IMG_SIZE, IMG_SIZE, 3))

    if len(detected) > 0:
        for i, face in enumerate(detected):
            # d = face['rect']
            x1, y1, x2, y2, w, h = face['x'], face['y'], face['x']+face['width'] + 1, face['y']+face['height'] + 1, face['width'], face['height']
            xw1 = max(int(x1 - MARGIN * w), 0)
            yw1 = max(int(y1 - MARGIN * h), 0)
            xw2 = min(int(x2 + MARGIN * w), img_w - 1)
            yw2 = min(int(y2 + MARGIN * h), img_h - 1)
            # cv2.rectangle(img, (x1, y1), (x2, y2), (255, 0, 0), 2)
            # cv2.rectangle(img, (xw1, yw1), (xw2, yw2), (255, 0, 0), 2)
            faces[i, :, :, :] = cv2.resize(img[yw1:yw2 + 1, xw1:xw2 + 1, :], (IMG_SIZE, IMG_SIZE))

        # predict ages and genders of the detected faces
        results = genderage_classifier.predict(faces)
        predicted_genders = results[0]
        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages_raw = results[1]
        # print ("predicted_ages", predicted_ages_raw, predicted_ages_raw.shape)
        predicted_ages = results[1].dot(ages).flatten()
        # print ("predicted_ages", predicted_ages, predicted_ages[0], type(predicted_ages[0]))
        ages = [float(x) for x in predicted_ages]
        for face, age in zip(detected, ages):
            face['age'] = age
        
        genders = [{'female': float(x[0]), 'male': float(x[1])} for x in predicted_genders]
        for face, gender in zip(detected, genders):
            if gender['male'] > .5:
                gender['label'] = 'male'
            else:
                gender['label'] = 'female'
            face['gender'] = gender
        # print ("predicted_genderage", data)

def face_to_dict (face):
    """ helper for JSON-serialisation """
    ret = {}
    ret['x'] = face['x']
    ret['y'] = face['y']
    ret['width'] = face['width']
    ret['height'] = face['height']
    if 'age' in face:
        ret['age'] = face['age']
    if 'emotion' in face:
        ret['emotion'] = face['emotion']
    if 'gender' in face:
        ret['gender'] = face['gender']
    if 'gender' in face:
        ret['gender'] = face['gender']
    return ret
