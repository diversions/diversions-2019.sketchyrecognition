#!/usr/bin/env python3
import cv2, os, sys, json
import argparse
import imutils
from time import sleep
from PIL import Image, ImageOps
import shutil
import datetime
from zbar import zbar
import re
from sketch_annotate import Predictor
from post import post, update, annotate


# if __name__ == "__main__":

def take_photo(camera, rotate, width, height, sleept=2.0, iso=0):
    print ("take photo", file=sys.stderr)
    if camera == -1:
        import picamera, picamera.array
        import RPi.GPIO as gpio
        gpio.output(4, gpio.HIGH)
        with picamera.PiCamera(resolution=(width, height)) as camera:
            # camera.resolution = ()
            # camera.iso = iso
            if sleept:
                # print ("sleep", sleep, file=sys.stderr)
                sleep(sleept)
            #ss = camera.exposure_speed
            #camera.shutter_speed = ss
            #camera.exposure_mode = 'off'
            #g = camera.awb_gains
            #camera.awb_mode = 'off'
            #camera.awb_gains = g

            with picamera.array.PiRGBArray(camera) as frame:
                camera.capture(frame, 'bgr')
                # print("got frame", frame)
                current_image_bgr = frame.array
                gray = cv2.cvtColor(current_image_bgr, cv2.COLOR_BGR2GRAY)
                if rotate:
                    gray = imutils.rotate_bound(gray, rotate)
                    current_image_bgr = imutils.rotate_bound(current_image_bgr, rotate)
                else:
                    current_image_bgr = imutils.rotate_bound(current_image_bgr, 0)
                current_image = gray
                # print('Captured %dx%d image' % (
                #         output.array.shape[1], output.array.shape[0]))
                # _, imdata = cv2.imencode(".jpg", gray)
                # data["height"], data["width"] = gray.shape[:2]
                # data["image"] = make_jpeg_data_url(imdata)
                # emit("photo", data)
                # return jsonify(data)
                return current_image, current_image_bgr
    else:
        stream = cv2.VideoCapture(camera)
        (grabbed, frame) = stream.read()
        current_image_bgr = frame
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        if rotate:
            gray = imutils.rotate_bound(gray, rotate)
            current_image_bgr = imutils.rotate_bound(current_image_bgr, rotate)
        current_image = gray
        _, imdata = cv2.imencode(".jpg", gray)
        # data={}
        # data["height"], data["width"] = gray.shape[:2]
        # data["image"] = make_jpeg_data_url(imdata)
        # emit("photo", data)
        stream.release()
        return current_image, current_image_bgr



ap = argparse.ArgumentParser("")
ap.add_argument("--camera", type=int, default=-1)
ap.add_argument("--width", type=int, default=2592)
ap.add_argument("--height", type=int, default=1944)
ap.add_argument("--rotate", type=int, default=0)
ap.add_argument("--vflip", default=False, action="store_true")
ap.add_argument("--models", default="./models", help="location of the models, default: ./models")
# ap.add_argument("--post", default=None, help="post images to given URL")
ap.add_argument('--gpiobutton', type=int, default=26, help="use given gpio pin as button")
# ap.add_argument("--audio", default=False, action="store_true", help="play the audio prompts")
args = ap.parse_args()


model = Predictor()

def p (msg=""):
    # if msg.strip():
    #     os.system("espeak \"{0}\" 2> /dev/null".format(msg))
    print (msg)

def tone ():
    os.system("aplay beep.wav 2> /dev/null")


def wait_for_button(pin):
    while True:
        button_state = GPIO.input(args.gpiobutton)
        if button_state == False:
            return True
        sleep(0.1)

if args.gpiobutton:
    import RPi.GPIO as GPIO

    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    # LIGHT
    GPIO.setup(4, GPIO.OUT)
    GPIO.setup(19, GPIO.OUT)  #BUTTONLED
    # BUTTON
    GPIO.setup(args.gpiobutton, GPIO.IN, pull_up_down=GPIO.PUD_UP)#Button to GPIO23

def fit_size (im, box):
    bw, bh = box
    iw, ih = im.size
    w = bw
    h = int((ih/iw) * w)
    if h <= bh:
        return (w, h)
    h = bh
    w = int((iw/ih) * h)
    return (w, h)

def extract_item_id (url):
    # http://sketchy.vandal.ist/i/32/
    m = re.search(r"\/i\/(\d+)\/", url)
    if m:
        return int(m.group(1))

p("all loaded")

try:
    while True:
        if args.gpiobutton:
            GPIO.output(19, True)
            wait_for_button(args.gpiobutton)
            GPIO.output(19, False)
        else:
            raw_input("Press THE BUTTON to take a photo")

        # LIGHT
        if args.gpiobutton:
            GPIO.output(4, GPIO.HIGH)

        current_image, current_image_bgr = take_photo(args.camera, args.rotate, args.width, args.height)
        current_image_rgb = cv2.cvtColor(current_image_bgr, cv2.COLOR_BGR2RGB)
        tone()

        if args.gpiobutton:
            GPIO.output(4, GPIO.LOW)

        n = datetime.datetime.now()
        base = n.strftime("%Y%m%d_%H%M%S")
        # print (datetime.datetime.now())

        image_fname = os.path.join("images", base + ".jpg")
        cv2.imwrite(image_fname, current_image_rgb)
        print ("image saved to {0}".format(image_fname), file=sys.stderr)
        url = zbar(image_fname)
        if url:
            item_id = extract_item_id(url)
            print ("DETECTED URL {0}, item_id:{1}".format(url, item_id))
        else:
            print ("NO URL detected")
            item_id = None

        resp = post(image_fname, item_id)
        # TODO: trim the image given the settings object (if present)
        if "settings" in resp:
            settings = resp['settings']
            x, y, right, bottom = settings['left'], settings['top'], settings['right'], settings['bottom']    
            w = right - x
            h = bottom - y
            trimmed_image = current_image_rgb[y:y+h, x:x+w]
            trimmed_image_fname = os.path.join("images", base + "cropped.jpg")
            cv2.imwrite(trimmed_image_fname, trimmed_image)
        else:
            trimmed_image_fname = image_fname

        # TRANFORM THE IMAGE
        tmp1 = os.path.join("images", base + ".01.jpg")
        os.system("./transform1.sh {0} {1}".format(trimmed_image_fname, tmp1))
        tmp2 = os.path.join("images", base + ".02.png")
        os.system("./transform2.sh {0} {1}".format(tmp1, tmp2))
        resp2 = update(tmp2, resp['update_url'])
        
        # PREDICT SKETCH ANNOTATION
        sketch_annotation = model.predict_sketch_category(tmp1)
        print ("sketch_annotation", sketch_annotation)
        annotate(sketch_annotation, resp2['annotate_url'])

        # SPEAKIT!
        script = ""
        first=True
        for label, prob in sketch_annotation:
            if first:
                script += "is it a {0}?".format(label)
            else:
                script += " or, is it a {0}?".format(label)
            first = False
        print (script)
        with open("script.txt", "w") as f:
            print(script, file=f)
        os.system("festival --tts script.txt")

        # os.system("./recognize.sh {0}".format(image_fname))
        # im = Image.fromarray(cv2.cvtColor(current_image_bgr,cv2.COLOR_BGR2RGB))
        sleep(1.0)

except KeyboardInterrupt:
    pass
finally:
    if args.gpiobutton:
        GPIO.output(4, GPIO.LOW)
        GPIO.output(19, False)
        GPIO.cleanup()
