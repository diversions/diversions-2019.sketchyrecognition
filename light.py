import RPi.GPIO as gpio
import time

import argparse
ap = argparse.ArgumentParser("")
ap.add_argument("--on", action="store_true", default=False)
ap.add_argument("--off", action="store_true", default=False)
ap.add_argument("--pin", type=int, default=4)
args = ap.parse_args()

pin = args.pin
gpio.setmode(gpio.BCM)
gpio.setwarnings(False)
gpio.setup(pin, gpio.OUT)
if args.on:
	gpio.output(pin, gpio.HIGH)
elif args.off:
	gpio.output(pin, gpio.LOW)
gpio.cleanup()
