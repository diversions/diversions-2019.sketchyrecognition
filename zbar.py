import subprocess
from xml.etree import ElementTree as ET 


def zbar (path):
	p = subprocess.Popen(["zbarimg", "--xml", path], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
	stdout, stderr = p.communicate("")
	t = ET.fromstring(stdout.decode("utf-8"))
	# print (t)
	data = t.find(".//{http://zbar.sourceforge.net/2008/barcode}data")
	if data is not None:
		return data.text
	else:
		return None
	# return stdout, stderr

if __name__ =="__main__":
	import argparse
	ap = argparse.ArgumentParser("")
	ap.add_argument("input")
	args = ap.parse_args()

	print(zbar(args.input))