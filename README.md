

Software
----------------

Installed with apt

* python3-caffe-cpu
* (zbartools) python-zbar + python-pil (for zbar support in python2 -- couldn't get working in python3)

Installed via pip:

* reportlab
* django
* qrcode



Wiring
------------
	_______
	1  2  |right-top edge
	3  4  |
	5  6
	7  8
	.  .
	.  .
	.  .
	.  .
	39 40 |

### Shutdown Button

Pins 14 & 16 (7th & 8th in case-side) (gnd + gpio23)


### Flash on Pi

GPIO -GREEN --BRN-- Pin7: GPIO4
GND   BLUE -- RED  Pin 6 : Ground
GND   PUR  -- ORA   not connected
5V -- GRAY -- YEL Pin 2: 5V

### Button

35 Yellow GPIO19
37 Green GPIO26
39 Blue GND


### Startup


### start.sh

chromium-browser --app="http://localhost/scan/" --window-position=0,0 --kiosk --disable-session-crashed-bubble --user-data-dir=/home/pi/browsers/1 &
chromium-browser --app="http://localhost/archive/" --window-position=1280,0 --kiosk --disable-session-crashed-bubble --user-data-dir=/home/pi/browsers
/2 &


### sketch.service
```
[Unit]
Description=Sketchy Recognition
After=network.target

[Service]
Type=simple
User=pi
WorkingDirectory=/home/pi/projects/sketchyrecognition
ExecStart=python3 cccv-loop.py
Restart=on-failure

[Install]
WantedBy=multi-user.target
```


src: https://www.dexterindustries.com/howto/run-a-program-on-your-raspberry-pi-at-startup/


### /etc/xdg/lxsession/LXDE-pi/autostart

```
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
@xscreensaver -no-splash
point-rpi
/home/pi/start.sh
```

src: https://www.wikihow.com/Execute-a-Script-at-Startup-on-the-Raspberry-Pi




