import requests, html5lib, json

def post (image, item_id, url="http://localhost/c/post/"):
    if type(image) == str:
        # path
        files = {'original': open(image, 'rb')}
    else:
        # assume imagedata, e.g. _, image = cv2.imencode(".jpg", image)
        files = {'original': ('image.jpg', image, 'image/jpeg', {'Expires': '0'})}
    form = requests.get(url)
    data = {}
    # print (form.text, file=sys.stderr)
    t = html5lib.parseFragment(form.text, namespaceHTMLElements=False)
    for hinput in t.findall(".//input[@type='hidden']"):
        if hinput.attrib.get("name") == "csrfmiddlewaretoken":
            data['csrfmiddlewaretoken'] = hinput.attrib.get("value")
    data['item_id'] = item_id
    # print ("pre-post check url", url)
    # print ("pre-post check data", data)
    # print ("pre-post check files", files)

    response = requests.post(url, \
        data=data, \
        cookies = form.cookies, \
        files=files)
    print ("response", response.text)
    # response = json.loads(response.text)
    return response.json()

def update (image, updateurl, baseurl="http://localhost"):
    if type(image) == str:
        # path
        files = {'trimmed': open(image, 'rb')}
    else:
        # assume imagedata, e.g. _, image = cv2.imencode(".jpg", image)
        files = {'trimmed': ('image.jpg', image, 'image/jpeg', {'Expires': '0'})}
    url = baseurl+updateurl
    form = requests.get(url)
    data = {}
    # print (form.text, file=sys.stderr)
    t = html5lib.parseFragment(form.text, namespaceHTMLElements=False)
    for hinput in t.findall(".//input[@type='hidden']"):
        if hinput.attrib.get("name") == "csrfmiddlewaretoken":
            data['csrfmiddlewaretoken'] = hinput.attrib.get("value")

    response = requests.post(url, \
        data=data, \
        cookies = form.cookies, \
        files=files)
    print ("response", response.text)
    # response = json.loads(response.text)
    return response.json()

def annotate (sketch_annotation, annotateurl, baseurl="http://localhost"):
    url = baseurl+annotateurl
    form = requests.get(url)
    data = {}
    # print (form.text, file=sys.stderr)
    t = html5lib.parseFragment(form.text, namespaceHTMLElements=False)
    for hinput in t.findall(".//input[@type='hidden']"):
        if hinput.attrib.get("name") == "csrfmiddlewaretoken":
            data['csrfmiddlewaretoken'] = hinput.attrib.get("value")
    data['sketch_annotation'] = json.dumps(sketch_annotation)

    response = requests.post(url, \
        data=data, \
        cookies = form.cookies)
    print ("response", response.text)
    # response = json.loads(response.text)
    return response.json()

if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser("")
    ap.add_argument("--url", default="http://localhost/c/post/")
    ap.add_argument("image")
    ap.add_argument("--imageurl", default=None)
    args = ap.parse_args()

    print (post(args.image, {}, args.url))
