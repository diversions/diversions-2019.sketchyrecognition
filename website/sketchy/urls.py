from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('i/<int:item_id>/', views.item, name='item'),
    path('i/<int:item_id>/qr', views.item_qr, name="item_qr"),
    path('i/<int:item_id>/pdf', views.item_pdf, name="item_pdf"),
    path('c/<int:cell_id>/', views.cell, name='cell'),
    path('c/editor/', views.cell_editor, name='cell_editor'),
    path('c/<int:cell_id>/update/', views.cell_update, name='cell_update'),
    path('c/<int:cell_id>/annotate/', views.cell_annotate, name='cell_annotate'),
    path('c/post/', views.cell_post, name='cell_post'),
    path('c/', views.cell_index, name='cell_index'),
    path('s/new/', views.settings_new, name='settings_new'),
    path('s/', views.settings, name='settings'),
    path('c/latest/', views.cell_latest, name='cell_latest'),
    path('scan/', views.scan, name='scan'),
    path('archive/', views.archive, name='archive'),
    path('a/<int:animation_id>/', views.animation, name='animation'),
    path('a/latest/', views.animation_latest, name='animation_latest'),
    path('a/', views.animation_index, name='animation_index'),
    path('pdf/', views.pdf, name="pdf"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
