from django.db import models
from django.conf import settings
from django.urls import reverse
from urllib.parse import urljoin
import json


class Item (models.Model):
    collectionName = models.CharField(max_length=255, blank=True)
    inventoryNb = models.CharField(max_length=255, blank=True)
    objectName = models.CharField(max_length=255, blank=True)
    objectTitle = models.CharField(max_length=255, blank=True)
    objectCulture = models.CharField(max_length=255, blank=True)
    geography = models.CharField(max_length=255, blank=True)
    dating = models.CharField(max_length=255, blank=True)
    material = models.CharField(max_length=255, blank=True)
    technique = models.CharField(max_length=255, blank=True)
    dimensions = models.CharField(max_length=255, blank=True)
    legalRightOwner = models.CharField(max_length=255, blank=True)
    url = models.URLField(blank=True)
    imageurl = models.URLField(blank=True)
    image = models.FileField(upload_to='items/', null=True, blank=True)
    contour = models.FileField(upload_to='items/', null=True, blank=True)

    lastmod = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{0}:{1}".format(self.collectionName, self.inventoryNb)

    def to_dict (self):
        return {
            'id': self.id,
            'inventoryNb': self.inventoryNb,
            'objectName': self.objectName,
            'objectTitle': self.objectTitle,
            'view_url': self.get_default_url(),
            'rurl': self.get_relative_url()
        }

    # def thumbnail (self):
    #     return "/media/thumbs/" + self.file.name.replace(".o.jpg", ".thumb.jpg")
    def get_default_url(self):
        return urljoin(settings.SITE_URL, reverse("item", args=(self.id, )))

    def get_relative_url(self):
        return reverse("item", args=(self.id, ))

class Setting (models.Model):
    left = models.IntegerField(null=False, default=0)
    top = models.IntegerField(null=False, default=0)
    right = models.IntegerField(null=False, default=0)
    bottom = models.IntegerField(null=False, default=0)
    command = models.CharField(blank=True, max_length=200)
    default = models.BooleanField(default=False)

    def to_dict (self):
        return {
            'left': self.left,
            'top': self.top,
            'right': self.right,
            'bottom': self.bottom
        }

class Animation (models.Model):
    class Meta:
        ordering = ['-time']
    time = models.DateTimeField(auto_now_add=True)
    item = models.ForeignKey(Item, on_delete=models.SET_NULL, related_name="animations", null=True, blank=True)
    visible = models.BooleanField(default=True)

    def cell_count (self):
        return self.cells.count()

    def cells_in_order (self):
        return self.cells.order_by("time")

    def to_dict (self, cells=False):
        ret = {}
        ret['url'] = self.get_relative_url()
        if self.item:
            ret['item'] = self.item.to_dict()
        if cells:
            cells = list(self.cells.order_by("time"))
            last_cell = cells[-1]
            ret['cells'] = [c.to_dict() for c in cells]
            if len(ret['cells']) > 0:
                qstr = "?cell={0}".format(len(cells))+"&"+last_cell.get_poll_query()
                ret['poll_url'] = self.get_relative_url() + qstr
            else:
                ret['poll_url'] = self.get_relative_url()
        else:
            ret['poll_url'] = self.get_relative_url()
        return ret

    def cells_dict (self):
        return self.to_dict(cells=True)

    def get_relative_url(self):
        return reverse("animation", args=(self.id, ))

    def firstcell (self):
        try:
            return self.cells.all()[0]
        except IndexError:
            return
class Cell (models.Model):
    class Meta:
        ordering = ['-time']
    original = models.FileField(upload_to='cells/', null=True, blank=True)
    trimmed = models.FileField(upload_to='cells/', null=True, blank=True)
    setting = models.ForeignKey(Setting, null=True, blank=True, on_delete=models.SET_NULL, related_name="cells")
    item = models.ForeignKey(Item, on_delete=models.SET_NULL, related_name="cells", null=True, blank=True)
    animation = models.ForeignKey(Animation, on_delete=models.SET_NULL, related_name="cells", null=True, blank=True)
    time = models.DateTimeField(auto_now_add=True)
    visible = models.BooleanField(default=True)
    sketch_annotation = models.CharField(blank=True, max_length=200)
    sketch_category = models.CharField(blank=True, max_length=50)
    sketch_certainty = models.FloatField(blank=True, default=0.0)

    def get_default_url(self):
        return urljoin(settings.SITE_URL, reverse("cell", args=(self.id, )))

    def get_relative_url(self):
        return reverse("cell", args=(self.id, ))

    def get_poll_query (self):
        if self.sketch_annotation:
            return "state=annotated"
        elif self.trimmed:
            return "state=trimmed"
        return ""

    def get_poll_url (self):
        ret = reverse("cell", args=(self.id, ))
        return ret + "?" + self.get_poll_query()

    def to_dict (self):
        ret = {}
        ret['view_url'] = self.get_relative_url()
        ret['poll_url'] = self.get_poll_url()
        if self.original:
            ret['original'] = self.original.url
        if self.trimmed:
            ret['trimmed'] = self.trimmed.url
        if self.sketch_annotation:
            ret['sketch_annotation'] = json.loads(self.sketch_annotation)
        return ret

    def sketch_annotation_parsed (self):
        if (self.sketch_annotation):
            return json.loads(self.sketch_annotation)
        else:
            return []

    def sketch_annotation_label (self):
        if (self.sketch_annotation):
            return json.loads(self.sketch_annotation)[0][0]

