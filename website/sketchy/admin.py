from django.contrib import admin

from .models import Item, Cell, Setting, Animation

class ItemAdmin(admin.ModelAdmin):
    list_display = ('inventoryNb', 'objectName', 'objectTitle', 'objectCulture', 'geography', 'dating', 'material')
    search_fields = ('inventoryNb', 'objectTitle')
    # fields = ['pub_date', 'question_text']
admin.site.register(Item, ItemAdmin)

class CellAdmin(admin.ModelAdmin):
    list_display = ('id', "time", 'original', 'item')
    raw_id_fields = ("item",)
    # search_fields = ('inventoryNb', 'objectTitle')
    # fields = ['pub_date', 'question_text']
admin.site.register(Cell, CellAdmin)

class SettingAdmin(admin.ModelAdmin):
    list_display = ('left', 'top', 'right', 'bottom', 'default')
    # raw_id_fields = ("item",)
    # search_fields = ('inventoryNb', 'objectTitle')
    # fields = ['pub_date', 'question_text']
admin.site.register(Setting, SettingAdmin)

class AnimationAdmin(admin.ModelAdmin):
    list_display = ('time', 'item', 'cell_count')
    raw_id_fields = ("item",)
admin.site.register(Animation, AnimationAdmin)
