from django.apps import AppConfig


class SketchyConfig(AppConfig):
    name = 'sketchy'
