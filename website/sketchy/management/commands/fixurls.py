from django.core.management.base import BaseCommand, CommandError
from sketchy.models import Item
import os
from urllib.parse import urlparse, urlunparse

class Command(BaseCommand):
    help = 'patch the urls'

    # def add_arguments(self, parser):
    #     parser.add_argument('csv')
    #     parser.add_argument('--imagepath')

    def handle(self, *args, **options):
        for item in Item.objects.all():
            if "/eMP/" not in item.url:
                p = urlparse(item.url)
                newurl = urlunparse(("https", p.hostname, "eMP"+p.path, None, p.query, None))
                print ("Patching\n{0}\n{1}\n".format(item.url, newurl))
                item.url = newurl
                item.save()