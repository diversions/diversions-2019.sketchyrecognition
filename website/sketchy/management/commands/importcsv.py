from django.core.management.base import BaseCommand, CommandError
from sketchy.models import Item
from django.conf import settings
import os
from csv import DictReader
from shutil import copyfile

class Command(BaseCommand):
    help = 'import CSV file'

    def add_arguments(self, parser):
        parser.add_argument('csv')
        parser.add_argument('--imagepath')

    def handle(self, *args, **options):
        csvpath = options['csv']
        folder, base = os.path.split(csvpath)
        base, ext = os.path.splitext(base)
        imagepath = options['imagepath'] or os.path.join(folder, base)
        with open(csvpath) as f:
            for row in DictReader(f):
                path, ext = os.path.splitext(row['image'])
                ifp = os.path.join(imagepath, path + ".trim.jpg")
                cfp = os.path.join(imagepath, path + ".contours.svg")
                if not os.path.exists(ifp):
                    print ("MISSING", ifp)
                    continue
                if not os.path.exists(cfp):
                    print ("MISSING", cfp)
                    continue
                
                ifp2 = os.path.join(settings.MEDIA_ROOT, "items", path+".jpg")
                cfp2 = os.path.join(settings.MEDIA_ROOT, "items", path+".contours.svg")

                item = Item()
                item.collectionName = row['collectionName']
                item.inventoryNb = row['inventoryNb']
                item.objectName = row['objectName']
                item.objectTitle = row['objectTitle']
                item.objectCulture = row['objectCulture']
                item.geography = row['geography']
                item.dating = row['dating']
                item.material = row['material']
                item.technique = row['technique']
                item.dimensions = row['dimensions']
                item.legalRightOwner = row['legalRightOwner']
                item.url = row['url']
                item.imageurl = row['imageurl']
                item.image = os.path.relpath(ifp2, settings.MEDIA_ROOT)
                item.contour = os.path.relpath(cfp2, settings.MEDIA_ROOT)
                item.save()
                copyfile(ifp, ifp2)
                copyfile(cfp, cfp2)

        # # Remove Rephoto objects with no file
        # delme = []
        # for p in Rephotograph.objects.all():
        #     if not os.path.exists(p.file.path):
        #         print ("missing file", p.file.url)
        #         delme.append(p)
        # for p in delme:
        #     p.delete()