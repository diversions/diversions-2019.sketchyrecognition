from django.core.management.base import BaseCommand, CommandError
from sketchy.models import Cell
from django.conf import settings
import os
from PIL import Image

def fit_size (iw, ih, bw, bh):
    w = bw
    h = int((ih/iw) * w)
    if h <= bh:
        return (w, h)
    h = bh
    w = int((iw/ih) * h)
    return (w, h)

class Command(BaseCommand):
    help = 'resize cells'

    def add_arguments(self, parser):
        parser.add_argument('--width', type=int, default=640)
        parser.add_argument('--height', type=int, default=480)

    def handle(self, *args, **options):
    	for c in Cell.objects.all():
    		print (c, c.trimmed)
    		if c.trimmed:
    			print (c.trimmed.path)
    			im = Image.open(c.trimmed.path)
    			iw, ih = im.size
    			nw, nh = fit_size(iw, ih, options['width'], options['height'])
    			im.thumbnail((nw, nh))
    			im.save(c.trimmed.path)
    			print ("resized image {0} to {1}x{2}".format(c.trimmed, im.size[0], im.size[1]))