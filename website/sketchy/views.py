from django.shortcuts import render, get_object_or_404, reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.core.paginator import Paginator

from django.forms import ModelForm
from django import forms

from sketchy.models import Item, Cell, Setting, Animation
import qrcode
from PIL import Image
################################
# REPORTLAB
import reportlab
from reportlab.pdfgen import canvas
from reportlab.lib.units import cm
# from reportlab.lib.utils import ImageReader
from reportlab.lib.pagesizes import A4
# from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
# from reportlab.platypus import Paragraph
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
# from reportlab.lib.colors import white, black, HexColor
# we know some glyphs are missing, suppress warnings
import reportlab.rl_config
#################################
from xml.etree import ElementTree as ET 
import re
import json


def fit_size (iw, ih, bw, bh):
    w = bw
    h = int((ih/iw) * w)
    if h <= bh:
        return (w, h)
    h = bh
    w = int((iw/ih) * h)
    return (w, h)

def index(request):
    s = request.GET.get('s', '').strip()
    if s:
        qq = Item.objects.filter(inventoryNb__icontains=s) | Item.objects.filter(objectName__icontains=s)
    else:
        qq = Item.objects.all()
    qq = qq.order_by("-lastmod", "objectTitle")
    paginator = Paginator(qq, 100)
    page = request.GET.get('page')

    ctx = {}
    ctx['s'] = s
    ctx['items'] = paginator.get_page(page)
    return render(request, "sketchy/index.html", ctx)

def item (request, item_id):
    item = get_object_or_404(Item, id=item_id)
    return render(request, "sketchy/item.html", {'item': item})

def item_qr (request, item_id):
    item = get_object_or_404(Item, id=item_id)
    im = qrcode.make(item.get_default_url(), border=0)  
    response = HttpResponse(content_type="image/png")
    im = im.resize((386, 386), resample=Image.NEAREST)
    im.save(response, "png")
    return response

def points_to_lines (points):
    lastpoint = None
    for x, y in points:
        if lastpoint:
            yield(lastpoint[0], lastpoint[1], x, y)
        lastpoint = x, y

def draw_item_page (c, item):
    border = (0*cm, 0*cm, 0*cm, 0*cm)
    pagewidth, pageheight = A4
    canvaswidth, canvasheight = pagewidth - (border[0]+border[2]), pageheight - (border[1]+border[3])
    # c.rect(border[0], border[3], canvaswidth, canvasheight)

    count = 0
    with open(item.contour.path) as f:
        et = ET.parse(f)
        root = et.getroot()
        width, height = int(root.attrib.get("width")), int(root.attrib.get("height"))
        if width > height:
            # ROTATE
            sw, sh = fit_size(width, height, canvasheight, canvaswidth)
            dx, dy = (canvasheight - sw)/2, (canvaswidth-sh)/2
        else:
            sw, sh = fit_size(width, height, canvaswidth, canvasheight)
            dx, dy = (canvaswidth - sw)/2, (canvasheight-sh)/2

        for contour in et.findall("{http://www.w3.org/2000/svg}polyline"):
            count += 1

        c.saveState()
        # c.setFont('Flux', 10)
        c.translate(border[0], border[3])
        if width > height:
            c.translate(canvaswidth, 0)
            c.rotate(90)
            c.translate(canvasheight, canvaswidth)
            c.rotate(180)
        else:        
            c.translate(canvaswidth, canvasheight)
            c.rotate(180)
        # print (width, height, canvaswidth, canvasheight, sw, sh)
        # print ("two ratios", sw/width, sh/height)
        c.translate(dx, dy)
        c.scale(sw/width, sh/height)
        # c.rect(0, 0, width, height)
        # c.setLineWidth(2)

        def to_point (x):
            x, y = x.split(",")
            return int(x), int(y)
        
        c.setLineWidth(2)
        for contour in et.findall("{http://www.w3.org/2000/svg}polyline"):
            pts = re.split(r"\s+", contour.attrib.get("points").strip())
            pts = [to_point(x) for x in pts]
            # c.lines(points_to_lines(pts))
            # print(pts)
            p = c.beginPath()
            p.moveTo(pts[0][0], pts[0][1])
            for pt in pts[1:]:
                p.lineTo(pt[0], pt[1])
            c.drawPath(p)

        # c.rect(0, 0, 100, 100)
        # c.line(0, 0, 10, 100)
        
        c.restoreState()

    # QR code
    im = qrcode.make(item.get_default_url(), border=0)  
    c.drawInlineImage(im, 0.5*cm, 0.5*cm, width=3.0*cm,height=3.0*cm)

def item_pdf (request, item_id):
    item = get_object_or_404(Item, id=item_id)
    # im = qrcode.make(item.get_default_url(), border=0)  
    response = HttpResponse(content_type="application/pdf")
    # im = im.resize((386, 386), resample=Image.NEAREST)
    # im.save(response, "png")
    reportlab.rl_config.warnOnMissingFontGlyphs = 0
    c = canvas.Canvas(response, pagesize=A4)
    c.setTitle(item.objectName)

    # LABEL
    # pdfmetrics.registerFont(TTFont('Flux', 'fonts/FluxischElse-Regular.ttf'))
    # c.saveState()
    # c.rotate(90)
    # c.setFont('Flux', 32)
    # c.drawString(3.5*cm, -1.5*cm, item.objectName)
    # c.restoreState()

    # CONTOURS
    # border = (3*cm, 1*cm, 3*cm, 1*cm)
    draw_item_page(c, item)
    c.showPage()
    c.save()
    return response

def pdf (request):
    items = Item.objects.order_by("?")[:100]
    # im = qrcode.make(item.get_default_url(), border=0)  
    response = HttpResponse(content_type="application/pdf")
    # im = im.resize((386, 386), resample=Image.NEAREST)
    # im.save(response, "png")
    reportlab.rl_config.warnOnMissingFontGlyphs = 0
    c = canvas.Canvas(response, pagesize=A4)
    c.setTitle("a vandalist coloring book")
    for item in items:
        draw_item_page(c, item)
        c.showPage()
    c.save()
    return response

class CellForm(ModelForm):
    # institution = forms.ModelChoiceField(queryset=Institution.objects.order_by("slug"), required=True)
    # code = forms.CharField(max_length=200, required=False)
    # age = forms.FloatField()

    # gender_male = forms.FloatField(min_value=0, max_value=1.0,
    #     widget=forms.NumberInput(attrs={'id': 'form_gender_male', 'step': "0.01"}))
    # ['angry','disgust','fear','happy','sad','surprise','neutral']
    # item_id = forms.CharField(widget=forms.Textarea)
    item_id = forms.CharField(widget=forms.TextInput, required=False)
    class Meta:
        model = Cell
        fields = ['original',]

def cell_post(request):
    if request.method == 'POST':
        form = CellForm(request.POST, request.FILES)
        if form.is_valid():
            # cd = json.loads(form.cleaned_data.get("sketch_annotation"))
            # print ("classification_data", cd)
            # REGISTER THE POST, validate form.instance
            form.save()

            resp = {'message': 'ok'}
            resp['update_url'] = reverse("cell_update", args=(form.instance.id,))
            item_id = form.cleaned_data.get("item_id")
            if item_id:
                item_id = int(item_id)
                item = Item.objects.get(pk=item_id)
                form.instance.item = item
                form.instance.save()
                resp['item'] = item.to_dict()

            # ASSOCIATE WITH AN ANIMATION, either existing or create one
            # get the last animation, if item the same ... and time diff not too great link
            # else start a new animation
            ANIMATION_TIME_SECS = 60*15
            use_animation = None
            try:
                last_animation = Animation.objects.order_by("-time")[0]
                if form.instance.item == None or last_animation.item == form.instance.item:
                    last_cell = last_animation.cells.order_by("-time")[0]
                    dt = form.instance.time - last_cell.time
                    resp['dt'] = dt.seconds
                    if (dt.days == 0 and dt.seconds < ANIMATION_TIME_SECS):
                        use_animation = last_animation
            except IndexError:
                pass

            if use_animation is None:
                use_animation = Animation(item=form.instance.item)
                use_animation.save()
                if form.instance.item:
                	form.instance.item.save() # hack to set lastmod !!! (nicolas ;)
            if use_animation.item:
                use_animation.item.save()
            resp['animation'] = use_animation.to_dict()

            form.instance.animation = use_animation
            form.instance.save()

            # the_cell = form.instance
            try:
                default = Setting.objects.get(default=True)
                resp['settings'] = default.to_dict()
            except Setting.DoesNotExist:
                default = None

            return HttpResponse(json.dumps(resp, indent=2), content_type="application/json")
            # return HttpResponseRedirect(reverse("home"))
    else:
        form = CellForm()
    return render(request, 'sketchy/cell_post.html', {'form': form})

class CellUpdateForm(ModelForm):
    # item_id = forms.CharField(widget=forms.TextInput)
    class Meta:
        model = Cell
        fields = ['trimmed',]

def cell_update(request, cell_id):
    cell = get_object_or_404(Cell, pk=cell_id)
    if request.method == 'POST':
        form = CellUpdateForm(request.POST, request.FILES, instance=cell)
        if form.is_valid():
            # cd = json.loads(form.cleaned_data.get("sketch_annotation"))
            # print ("classification_data", cd)
            # REGISTER THE POST, validate form.instance
            form.save()
            resp = {'message': 'ok'}
            resp['annotate_url'] = reverse("cell_annotate", args=(form.instance.id,))
            return HttpResponse(json.dumps(resp, indent=2), content_type="application/json")
    else:
        form = CellUpdateForm(instance=cell)
    return render(request, 'sketchy/cell_update.html', {'form': form})


class CellAnnotateForm(ModelForm):
    # item_id = forms.CharField(widget=forms.TextInput)
    class Meta:
        model = Cell
        fields = ['sketch_annotation']

def cell_annotate(request, cell_id):
    cell = get_object_or_404(Cell, pk=cell_id)
    if request.method == 'POST':
        form = CellAnnotateForm(request.POST, request.FILES, instance=cell)
        if form.is_valid():
            # cd = json.loads(form.cleaned_data.get("sketch_annotation"))
            # print ("classification_data", cd)
            # REGISTER THE POST, validate form.instance
            form.save()
            resp = {'message': 'ok'}
            return HttpResponse(json.dumps(resp, indent=2), content_type="application/json")
    else:
        form = CellAnnotateForm(instance=cell)
    return render(request, 'sketchy/cell_annotate.html', {'form': form})


def cell(request, cell_id):
    cell= get_object_or_404(Cell, pk=cell_id)
    return render(request, "sketchy/cell.html", {'cell': cell})

def cell_index(request):
    cells= Cell.objects.all()
    return render(request, "sketchy/cell_index.html", {'cells': cells})

def cell_editor(request):
    cells= Cell.objects.all()
    paginator = Paginator(cells, 1)
    page = request.GET.get('i')
    ctx = {}
    ctx['cells'] = paginator.get_page(page)
    return render(request, "sketchy/cell_editor.html", ctx)

def cell_latest(request):
    lastcell= Cell.objects.order_by("-time")[0]
    return HttpResponse(json.dumps(lastcell.to_dict(), indent=2), content_type="application/json")

def settings_new (request):
    lastcell = Cell.objects.order_by("-time")[0]
    im = Image.open(lastcell.original.path)
    w, h = im.size

    try:
        default = Setting.objects.get(default=True)
        left = int(request.GET.get("left", default.left))
        top = int(request.GET.get("top", default.top))
        right = int(request.GET.get("right", default.right))
        bottom = int(request.GET.get("bottom", default.bottom))
    except Setting.DoesNotExist:
        default = None
        left = int(request.GET.get("left", "0"))
        top = int(request.GET.get("top", "0"))
        right = int(request.GET.get("right", "0"))
        bottom = int(request.GET.get("bottom", "0"))


    if request.GET.get("c.x"):
        x = int(request.GET.get("c.x"))
        if x < w/2:
            left = x
        else:
            right = x
    if request.GET.get("c.y"):
        y = int(request.GET.get("c.y"))
        if y < h/2:
            top = y
        else:
            bottom = y

    if request.GET.get("_submit") == "save":
        try:
            default = Setting.objects.get(default=True)
            default.default = False
            default.save()
        except Setting.DoesNotExist:
            default = None

        s = Setting(left=left, top=top, right=right, bottom=bottom, default=True)
        s.save()
        return HttpResponseRedirect(reverse("settings"))
    return render(request, "sketchy/settings_new.html", {'cell': lastcell, 'left': left, 'top': top, 'right': right, 'bottom': bottom, 'width': right - left, 'height': bottom - top})

def settings (request):
    try:
        default = Setting.objects.get(default=True)
    except Setting.DoesNotExist:
        default = None
    return render(request, "sketchy/settings.html", {"default": default})

def scan(request):
    return render(request, "sketchy/scan.html", {})

def archive(request):
    return render(request, "sketchy/archive.html", {})

def animation(request, animation_id):
    animation = get_object_or_404(Animation, pk=animation_id)
    data = json.dumps(animation.cells_dict())
    return render(request, "sketchy/animation.html", {'animation': animation, 'cells': data})

def animation_index(request):
    animations = Animation.objects.all()
    paginator = Paginator(animations, 25)
    page = request.GET.get('page')
    ctx = {}
    ctx['animations'] = paginator.get_page(page)
    return render(request, "sketchy/animation_index.html", ctx)

def animation_latest(request):
    latest = Animation.objects.order_by("-time")[0]
    return HttpResponse(json.dumps(latest.to_dict(cells=True), indent=2), content_type="application/json")
